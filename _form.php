<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtml;
use common\models\Demndeur;

/* @var $this yii\web\View */
/* @var $model common\models\Champ */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="champ-form">

    <div class="col-md-4">

    <?php $form = ActiveForm::begin(); ?>
   
    <?= $form->field($model, 'id_demndeur')->dropDownList( ArrayHelper::map(
        Demndeur::find()->all(),'id' , 'nom') ,
            ['prompt'=>'Select Domndeur'] )  ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row"><div class="col-md-6"><!-- id champ-lat -->
    <?= $form->field($model, 'lat')->textInput() ?>
    </div><div class="col-md-6"><!-- id champ-lng -->
    <?= $form->field($model, 'lng')->textInput() ?>
    </div>
    
    <?= $form->field($model,'superficie')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
   <?php ActiveForm::end(); ?>

</div>

</div>

<div class="col-md-8">

<?php 

    // first lets setup the center of our map
    $center = new \dosamigos\leaflet\types\LatLng(['lat' => 51.508, 'lng' => -0.11]); 
    
    // now lets create a marker that we are going to place on our map
    $marker = new \dosamigos\leaflet\layers\Marker(['latLng' => $center, 'popupContent' => 'Echantillon N: 1<br> (x1=5cm , x2=10 cm ,x3=15cm , x4=20cm) ']);
    
    // The Tile Layer (very important)
    $tileLayer = new \dosamigos\leaflet\layers\TileLayer([
       'urlTemplate' =>'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicmFjaGlkMTIzIiwiYSI6ImNrNHg4b3FxYTA0bWYzbnBjMWpwY3RpazEifQ.a7rfnrx5MS1wvnWA53fWGw',
        'clientOptions' => [
            'attribution' => 'Tiles Courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png">, Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            'id' => 'mapbox.satellite' ,
            'subdomains'  => [ '1' ,'2' ,'3' ,'4' ],
        ]
    ]);

    // now our component and we are going to configure it
    $leaflet = new \dosamigos\leaflet\LeafLet([
        'center' => $center, // set the center
    ]);

    // init the 2amigos leaflet plugin provided by the package
    $drawFeature = new \davidjeddy\leaflet\plugins\draw\Draw();
    // optional config array for leadlet.draw
    $drawFeature->options = [
        "position" => "topright",
        "draw" => [
            "polyline" => [
                "shapeOptions" => [
                    "color" => "#ff0000",
                    "weight" => 10 ,
                ]
            ],
            "polygon" => [ 
                "allowIntersection" => false, // Restricts shapes to simple polygons
                "drawError" => [
                    "color" => "#e1e100", // Color the shape will turn when intersects
                    "message" => "<b>Oh snap!</b> you can't draw that!" // Message that will show when intersect
                ],
                "shapeOptions" => [
                    "color" => "#bada55"
                ]
            ],
            "circle" => true, // Turns off this drawing tool
            "rectangle" => [
                "shapeOptions" => [
                    "clickable" => false
                ]

            ]
        ]
    ];

    // Different layers can be added to our map using the `addLayer` function.
    $leaflet->addLayer($marker)             // add the marker
            ->addLayer($tileLayer)  
            //->addLayer($polygon)        // add the tile layer
            ->installPlugin($drawFeature);  // add draw plugin
        
    // we could also do 

    echo $leaflet->widget(['options' => ['style' => 'min-height: 450px']]);

//json::encode($drawFeature->options);

      ?> 

</div>

<script >
</script>


<?php

$this->registerJs("
    $(document).on('create','create',function(){
    var  test = 'this is an ajax test' ;
    $.ajax({
        url: '  ". Yii::$app->request->baseUrl . " champ/test' ,
        type: 'POST',
        dataType: 'json',
        data: {test : test },
        success: function (data) {
            console.log(data);
        },
    });
})"); 

 ?>

</div>