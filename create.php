<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Champ */

$this->title = 'Information sur le Champ';
$this->params['breadcrumbs'][] = ['label' => 'Champs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="echantillon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
